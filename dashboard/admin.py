from django.contrib import admin
from .models import *
# Register your models here.
admin.site.register(Job)
admin.site.register(Internship)
admin.site.register(UserJobApplied)
admin.site.register(UserInternshipApplied)
admin.site.register(Employee)
