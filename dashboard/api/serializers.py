from dashboard.models import *
from rest_framework import serializers


class EmployeeSerializer(serializers.ModelSerializer):
	class Meta:
		model = Employee
		fields = '__all__'


#Job
class JobSerializer(serializers.ModelSerializer):
	class Meta:
		model = Job
		fields = '__all__'
		read_only_fields = ('id','created_at','updated_at',)

class UserJobAppliedSerializer(serializers.ModelSerializer):
	class Meta:
		model = UserJobApplied
		fields ='__all__'



class UserDetailSerializer(serializers.ModelSerializer):
	class Meta:
		model = User
		fields = '__all__'

class UserJobAppliedUpdateSerializer(serializers.ModelSerializer):
	pending = serializers.BooleanField()
	select = serializers.BooleanField()
	shortlist = serializers.BooleanField()
	decline = serializers.BooleanField()
	
	class Meta:
		model = UserJobApplied
		# fields = "__all__"
		fields =('pending','select','shortlist','decline',)


# Internship


class InternshipSerializer(serializers.ModelSerializer):


	class Meta:
		model = Internship
		fields = '__all__'
		read_only_fields = ('id','created_at','updated_at',)

class UserInternshipAppliedSerializer(serializers.ModelSerializer):
	class Meta:
		model = UserInternshipApplied
		fields ='__all__'
		

class UserIntDetailSerializer(serializers.ModelSerializer):
	class Meta:
		model = Internship
		fields = '__all__'





