from django.urls import path,include
from django.contrib import admin
from django.conf import settings
from dashboard.api.views import (
	EmployeeAPI,
	JobDetail,
	updateStatus,
	JobListAPIView,
	JobCreateAPIView,
	JobAppliedCreateAPIView,
	JobAppliedListAPIView,
	# JobJsonAPI,
	JobappliedRetriveAPI,
	UserDetailRetriveAPI,

	InternshipDetail,
	InternshipListAPIView,
	InternshipCreateAPIView,
	InternshipAppliedCreateAPIView,
	InternshipAppliedListAPIView,
	JobappliedRetriveAPI,
	internUpdateStatus,
	UserInternDetailRetriveAPI,
	InternshipappliedRetriveAPI,
	get_job_status,
	get_intern_status,
	TotalUserCountAPI,
	TotalInternshipUserCountAPI,
	TotalJobUserCountAPI,
	TotalInternUsers,
	TotalJobUsers,
	internhipclose,
	jobclose,
)



urlpatterns=[

#employee

			path('employeeAPI/',EmployeeAPI.as_view()),

#Job

			path('jobcreateapi/',JobCreateAPIView.as_view()),
			path('joblistapi/',JobListAPIView.as_view()),
			path('jobdetailapi/<slug:slug>',JobDetail.as_view()),

			path('jobappliedcreate/',JobAppliedCreateAPIView.as_view()),
			path('jobappliedlist/',JobAppliedListAPIView.as_view()),

			# path('jobappliedjson/<int:pk>',JobJsonAPI.as_view()),
			path('JobappliedRetriveAPI/<int:id>',JobappliedRetriveAPI.as_view()),

			path('UserDetailRetriveApi/<int:pk>',UserDetailRetriveAPI.as_view()),
			# path('JobAppliedUpdateAPIView/',JobAppliedUpdateAPIView),
			path('update_status/', updateStatus),

#Internship
			
			path('internshipcreateapi/',InternshipCreateAPIView.as_view(), name="list"),
			path('internshiplistapi/',InternshipListAPIView.as_view(),  name="detail"),
			path('internshipdetailapi/<slug:slug>',InternshipDetail.as_view(), name="update"),

			path('internappliedcreate/',InternshipAppliedCreateAPIView.as_view()),
			path('internappliedlist/',InternshipAppliedListAPIView.as_view()),

			# path('jobappliedjson/<int:pk>',JobJsonAPI.as_view()),
			path('internappliedRetriveAPI/<int:id>',InternshipappliedRetriveAPI.as_view()),

			path('userinterndetailRetriveApi/<int:pk>',UserInternDetailRetriveAPI.as_view()),

			path('intern_update_status/', internUpdateStatus),

			path('user_job_status/<int:id>/', get_job_status),

			path('user_intern_status/<int:id>/', get_intern_status),

			
			path('total_count_user/<int:id>/', TotalUserCountAPI.as_view()),
			
			path('total_internship_user_count/<int:id>',TotalInternshipUserCountAPI.as_view()),

			path('total_job_user_count/<int:id>',TotalJobUserCountAPI.as_view()),
			
			path('api/total_Intern_applicant/',TotalInternUsers.as_view()),

			path('api/total_job_applicant/', TotalJobUsers.as_view()),


			path('intern_closejob/<int:id>',internhipclose),

			path('close_job/<int:id>',jobclose),

			
	
	]