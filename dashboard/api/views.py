from django.contrib.auth import get_user_model
from django.db.models import Q
from django.http import HttpResponse
from django.http import JsonResponse
from rest_framework.permissions import IsAuthenticatedOrReadOnly,IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from ..models import Employee, Job, Internship,UserJobApplied ,UserInternshipApplied

from  .serializers import (
	JobSerializer,
	UserJobAppliedSerializer,
	UserDetailSerializer,
	EmployeeSerializer,
	# UserJobAppliedUpdateSerializer,
#Internship
	InternshipSerializer,
	UserIntDetailSerializer,
	UserInternshipAppliedSerializer,
	)	
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from rest_framework import generics,status
from rest_framework import serializers,permissions
from django.core import serializers



User=get_user_model()

from rest_framework.generics import (
		RetrieveAPIView,
		UpdateAPIView,
		DestroyAPIView,
		ListAPIView,
		CreateAPIView,
		)

#employee 

class EmployeeAPI(APIView):
	
	def post(self,request):

		serializer = EmployeeSerializer(data=request.data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data)
		return Response(serializer.errors)

	def get(self,request):
		queryset = Employee.objects.all()
		serializer = EmployeeSerializer(queryset, many=True)
		return Response(serializer.data)




#job
class JobDetail(generics.RetrieveUpdateDestroyAPIView):
	# permission_classes = (IsAuthenticatedOrReadOnly,)
	queryset = Job.objects.all()
	serializer_class = JobSerializer
	lookup_field = 'slug'

class JobListAPIView(ListAPIView):
	permission_classes = (IsAuthenticatedOrReadOnly,)
	queryset = Job.objects.all()
	serializer_class = JobSerializer

	
class JobCreateAPIView(CreateAPIView):
	# permission_classes = (IsAuthenticated,)
	queryset = Job.objects.all()
	serializer_class = JobSerializer

	
		


# UserDetail Retreive Api view
class UserDetailRetriveAPI(RetrieveAPIView):
	# permission_classes = (IsAuthenticatedOrReadOnly,)
	queryset = User.objects.all()
	serializer_class = UserDetailSerializer


#job applied api view

class JobappliedRetriveAPI(APIView):
	def get(self, request, id, format=None):

		btnType = request.GET.get('type')
		
		name = [user.user_id.username for user in UserJobApplied.objects.filter(job_id=id)]
		_id = [user.user_id.id for user in UserJobApplied.objects.filter(job_id=id)]
		data = list(zip(_id, name))
		return Response(data)
	

	
class JobAppliedCreateAPIView(CreateAPIView):
	queryset = UserJobApplied.objects.all()
	serializer_class  = UserJobAppliedSerializer

	# def get(self,request):
	# 	user=request.GET.get('user')
	# 	job = request.GET.get('job')
	# 	print(user,job)
	# 	return HttpResponse('done')

	# 	user = UserJobApplied.objects.filter(user_id=id)
	# 	job = UserJobApplied.objects.filter(job_id=id)
	# 	if user==job:
	# 		return False
	# 	else:
	# 		return True


class JobAppliedListAPIView(ListAPIView):
	permission_classes = (IsAuthenticatedOrReadOnly,)
	queryset = UserJobApplied.objects.all()
	serializer_class = UserJobAppliedSerializer


class JobJsonAPI(APIView):
	serializer_class = UserJobAppliedSerializer
	
	def get(self,request,pk, format=None):
		# user=UserJobApplied.objects.filter(job=pk)

		user= UserJobApplied.objects.all().order_by('id')
		posts_serialized = serializers.serialize('json',user)
		return JsonResponse(posts_serialized, safe=False)
		
		print('----------',user)
		return JsonResponse(json.dumps(user))



class TotalUserCountAPI(APIView):
	
	def get(self,request,id, *args , **kwargs):
		obj1 = UserJobApplied.objects.filter(job_id=id, pending=True ).count()
		obj2 = UserJobApplied.objects.filter(job_id=id, shortlist=True ).count()
		obj3 = UserJobApplied.objects.filter(job_id=id, select=True ).count()
		obj4 = UserJobApplied.objects.filter(job_id=id, decline=True ).count()
		obj5 = UserJobApplied.objects.filter(job_id=id ).count()
		result ={"pending":obj1, "shortlist":obj2, "select":obj3, "decline":obj4,'total':obj5}
		
		return Response(result)

# Total Internship Count User
class TotalInternshipUserCountAPI(APIView):
	
	def get(self,request,id, *args , **kwargs):
		obj1 = UserInternshipApplied.objects.filter(internship_id=id, pending=True ).count()
		obj2 = UserInternshipApplied.objects.filter(internship_id=id, shortlist=True ).count()
		obj3 = UserInternshipApplied.objects.filter(internship_id=id, select=True ).count()
		obj4 = UserInternshipApplied.objects.filter(internship_id=id, decline=True ).count()
		obj5 = UserInternshipApplied.objects.filter(internship_id=id).count()
		result ={"pending":obj1, "shortlist":obj2, "select":obj3, "decline":obj4,'total':obj5}
		
		return Response(result)
# Total Job User Count
class TotalJobUserCountAPI(APIView):
	def get(self,request,id,*args,**kwargs):
		obj1 = UserJobApplied.objects.filter(job_id=id,pending=True).count()
		obj2 = UserJobApplied.objects.filter(job_id=id,shortlist=True).count()
		obj3 = UserJobApplied.objects.filter(job_id=id,select=True).count()
		obj4 = UserJobApplied.objects.filter(job_id=id,decline=True).count()
		obj5 = UserJobApplied.objects.filter(job_id=id).count()
		response={"pending":obj1,"shortlist":obj2,"select":obj3,"decline":obj4,"total":obj5}
		return Response(response)

class TotalInternUsers(APIView):
	def get(self,request):
		obj1 = UserInternshipApplied.objects.all().count()
		obj2 = UserInternshipApplied.objects.filter(select=True).count()

		obj3 = Internship.objects.all().count()
		response={"total_applicant":obj1,"total_selected":obj2,"total_job":obj3}
		return Response(response)

class TotalJobUsers(APIView):
	def get(self,request):
		obj1 = UserJobApplied.objects.all().count()
		obj2 = UserJobApplied.objects.filter(select=True).count()
		obj3 = Job.objects.all().count()
		response={"total_applicant":obj1,"total_selected":obj2,"total_job":obj3}
		return Response(response)
	
# Update Status 

def updateStatus(request):
	btntype = request.GET.get('type')
	jobId = request.GET.get('job')
	userId = request.GET.get('user')

	data = UserJobApplied.objects.filter(Q(user_id=userId) & Q(job_id=jobId))
	
	response = {}

	if data.exists():
		setStatus(btntype, data)
		response['status'] = "Success"
		response['action'] = btntype

	else:
		response['status'] = "Error"
		response['action'] = btntype

	return JsonResponse(response)




# Change Internship status
def internUpdateStatus(request):
	btntype = request.GET.get('type')
	userId = request.GET.get('user')
	internId = request.GET.get('intern')


	data = UserInternshipApplied.objects.filter(Q(user_id=userId) & Q(internship_id=internId))

	response = {}
	
	if data.exists():
		setStatus(btntype, data)
		response['status']='Success'
		response['action'] = btntype
	else:
		response['status']= 'Error'
		response['action'] = btntype
	return JsonResponse(response)



#  Helper function
def setStatus(type, data):
	if type == 'shortlist':
		for d in data:
			d.pending=False
			d.shortlist= True
			d.select= False
			d.decline= False
			d.save()

	elif type == 'select':
		for d in data:
			d.pending=False
			d.shortlist= False
			d.select= True
			d.decline= False
			d.save() 
	
	elif type == 'decline':
		for d in data:
			d.pending=False
			d.shortlist= False
			d.select= False
			d.decline= True
			d.save() 
	
	else:
		for d in data:
			d.pending=True
			d.shortlist= False
			d.select= False
			d.decline= False
			d.save()


#User Current Status

def get_job_status(request, id):

		btnType = request.GET.get('type')
		print('---------------',type)
		
		_id = []
		name = []
		
		if btnType == 'pending':
			for user in UserJobApplied.objects.filter(job_id=id ,pending =True):
				name.append(user.user_id.username)
				_id.append(user.user_id.id)
			
		if btnType == 'shortlist':
			for user in UserJobApplied.objects.filter(job_id=id, shortlist = True):
				name.append(user.user_id.username)
				_id.append(user.user_id.id)

		
		if btnType == 'select':
			for user in UserJobApplied.objects.filter(job_id=id, select =  True):
				name.append(user.user_id.username)
				_id.append(user.user_id.id)

		if btnType == 'decline':
			for user in UserJobApplied.objects.filter(job_id=id, decline =  True):
				name.append(user.user_id.username)
				_id.append(user.user_id.id)
		
		data = list(zip(_id, name))
		return JsonResponse({"data":data})


def get_intern_status(request, id):

		btnType = request.GET.get('type')
		print('---------------',type)
		
		_id = []
		name = []
		
		if btnType == 'pending':
			for user in UserInternshipApplied.objects.filter(internship_id=id ,pending =True):
				name.append(user.user_id.username)
				_id.append(user.user_id.id)
			
			
		if btnType == 'shortlist':
			for user in UserInternshipApplied.objects.filter(internship_id=id ,shortlist=True):
				name.append(user.user_id.username)
				_id.append(user.user_id.id)

		
		if btnType == 'select':
			for user in UserInternshipApplied.objects.filter(internship_id=id ,select=True):
				name.append(user.user_id.username)
				_id.append(user.user_id.id)

		if btnType == 'decline':
			for user in UserInternshipApplied.objects.filter(internship_id=id, decline=True):
				name.append(user.user_id.username)
				_id.append(user.user_id.id)
		
		data = list(zip(_id, name))
		return JsonResponse({"data":data})

#internship close job

def internhipclose(request,id):

	data = Internship.objects.filter(Q(id=id))
	response = {}

	if data.exists():
		status = close_internship(data)

		if status:
			response['status'] = status
			response['msg'] = "Job Disabled"

		else:
			response['status'] = status
			response['msg'] = "Job Enabled"

	return JsonResponse(response)

def close_internship(data):
	status = False
	for d in data:
		if d.status:
			d.status = False
			status = True
		else:
			d.status = True
		d.save()

	return status
	

#Job close 

def jobclose(request,id):

	data = Job.objects.filter(Q(id=id))
	response = {}

	if data.exists():
		status = close_job(data)

		if status:
			response['status'] = status
			response['msg'] = "Job Disabled"

		else:
			response['status'] = status
			response['msg'] = "Job Enabled"

	return JsonResponse(response)

def close_job(data):
	status = False
	for d in data:
		if d.status:
			d.status = False
			status = True
		else:
			d.status = True
		d.save()

	return status
	
  
	

#internship

class InternshipDetail(generics.RetrieveUpdateDestroyAPIView):
	# permission_classes = (IsAuthenticatedOrReadOnly,)
	queryset = Internship.objects.all()
	serializer_class = InternshipSerializer
	lookup_field = 'slug'

class InternshipListAPIView(ListAPIView):
	permission_classes = (IsAuthenticatedOrReadOnly,)
	queryset = Internship.objects.all()
	serializer_class = InternshipSerializer


class InternshipCreateAPIView(CreateAPIView):
	# permission_classes = (IsAuthenticatedOrReadOnly,)
	queryset = Internship.objects.all()
	serializer_class = InternshipSerializer



class UserInternDetailRetriveAPI(RetrieveAPIView):
	# permission_classes = (IsAuthenticatedOrReadOnly,)
	queryset = User.objects.all()
	serializer_class = UserDetailSerializer
	


class InternshipappliedRetriveAPI(APIView):
	def get(self, request, id, format=None):
		name = [user.user_id.username for user in UserInternshipApplied.objects.filter(internship_id=id)]
		_id = [user.user_id.id for user in UserInternshipApplied.objects.filter(internship_id=id)]
		data = list(zip(_id, name))
		return Response(data)


	
class InternshipAppliedCreateAPIView(CreateAPIView):
	queryset = UserInternshipApplied.objects.all()
	serializer_class  = UserInternshipAppliedSerializer

class InternshipAppliedListAPIView(ListAPIView):
	permission_classes = (IsAuthenticatedOrReadOnly,)
	queryset = UserJobApplied.objects.all()
	serializer_class = UserJobAppliedSerializer


class JobJsonAPI(APIView):
	serializer_class = UserJobAppliedSerializer
	
	def get(self,request,pk, format=None):
		# user=UserJobApplied.objects.filter(job=pk)

		user= UserJobApplied.objects.all().order_by('id')
		posts_serialized = serializers.serialize('json',user)
		return JsonResponse(posts_serialized, safe=False)
		
		print('----------',user)
		return JsonResponse(json.dumps(user))



























#For Json

# class JobJsonAPI(APIView):
	
# 	serializer_class = UserJobAppliedSerializer
	
# 	def get(self,request,pk, format=None):
# 		# user=UserJobApplied.objects.filter(job=pk)

# 		user= UserJobApplied.objects.all().order_by('id')
# 		posts_serialized = serializers.serialize('json',user)
# 		return JsonResponse(posts_serialized, safe=False)
	
		
# 		print('----------',user)
		
# 		return JsonResponse(json.dumps(user))




