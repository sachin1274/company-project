from .models import Job , Internship
from django.contrib.admin.widgets import AdminDateWidget
from django.forms.fields import DateField
from django import forms
from django.forms import ModelForm
from django.forms import widgets
# from ckeditor.fields import RichTextField
from ckeditor.widgets import CKEditorWidget
intustry_choice=(('IT','it'),('Non IT','non it'))
paid_choices=(('Yes','yes'),('No','no'))
class Job_form(forms.ModelForm):

    class Meta:
        model=Job
        fields="__all__"
        # paid = forms.ChoiceField(widget=forms.RadioSelect(), choices=paid_choices)

        labels = {
            "title": "Job title",
            

        }




        #db_table = 'job'
        # verbose_name = 'job'
        # verbose_name_plural = 'jobs'


class Internship_form(forms.ModelForm):
    # title           = forms.CharField(max_length=120)
    # #slug            = models.SlugField(unique=True, null=True, blank=True)
    # profile         = forms.CharField(max_length=50,widget=forms.TextInput(attrs={'class':'form-control','placeholder':"hello"}))
    # location        = forms.CharField(max_length=100,widget=forms.TextInput(attrs={'class':'form-control','placeholder':"hello"}))
    # city            = forms.CharField(max_length=100,widget=forms.TextInput(attrs={'class':'form-control','placeholder':"hello"}))
    # skills          = forms.CharField(max_length=100,widget=forms.TextInput(attrs={'class':'form-control','placeholder':"hello"}))
    # start_date      = forms.DateTimeField()
    # last_date       = forms.DateTimeField()
    # stipend         = forms.FloatField()
    # duration        = forms.CharField(max_length=20,widget=forms.TextInput(attrs={'class':'form-control','placeholder':"hello"}))
    # description     = forms.CharField(max_length=300,widget=forms.TextInput(attrs={'class':'form-control','placeholder':"hello"}))
    # no_of_seats     = forms.IntegerField(widget=forms.TextInput(attrs={'class':'form-control','placeholder':"hello"}))
    # linkedin_id     = forms.CharField(max_length=100,widget=forms.TextInput(attrs={'class':'form-control','placeholder':"hello"}))
    # interview_date  = forms.DateTimeField()
    # about_interview = forms.CharField(max_length=500,widget=forms.TextInput(attrs={'class':'form-control','placeholder':"hello"}))
    # quality_council = forms.CharField(max_length=1000,widget=forms.TextInput(attrs={'class':'form-control','placeholder':"hello"}))
    # who_can_apply   = forms.CharField(max_length=1000,widget=forms.TextInput(attrs={'class':'form-control','placeholder':"hello"}))
    # #user            = models.ForeignKey(User, on_delete=models.CASCADE)
    # status          = forms.BooleanField()

    class Meta:
        model=Internship
        fields="__all__"
        #db_table = 'job'
