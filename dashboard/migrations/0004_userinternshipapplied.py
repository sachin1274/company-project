# Generated by Django 2.2 on 2019-05-18 07:54

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('dashboard', '0003_auto_20190518_0021'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserInternshipApplied',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('pending', models.BooleanField()),
                ('shortlist', models.BooleanField()),
                ('select', models.BooleanField()),
                ('decline', models.BooleanField()),
                ('internship_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='dashboard.Internship')),
                ('user_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'userinternapplied',
                'verbose_name_plural': 'userinternapplieds',
            },
        ),
    ]
