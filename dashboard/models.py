from django.db import models
from ckeditor.fields import RichTextField
from .slugify import unique_slug_generator
from django.db.models.signals import pre_save
from django.utils.text import slugify
from django.contrib.auth import get_user_model
from datetime import datetime

User=get_user_model()

class Employee(models.Model):

    first_name  = models.CharField(max_length=50)
    last_name   = models.CharField(max_length=30)
    email       = models.EmailField(max_length=255)
    phone       = models.IntegerField()
    username    = models.CharField(max_length=30)
    password    = models.CharField(max_length=255)
    user_type   = models.IntegerField(default=4)
    user_parent = models.ForeignKey(User,on_delete=models.CASCADE,null=True,blank=True)     
    
    
    class Meta:
        verbose_name = 'employee'
        verbose_name_plural = 'employees'
    def __str__(self):
        return self.first_name


class Job(models.Model):
    # id              =models.PositiveIntegerField(primary_key=True,default=datetime.now(),editable=False)
    title           = models.CharField(max_length=120)
    slug            = models.SlugField(unique=True, null=True, blank=True)
    profile         = models.CharField(max_length=50)
    nature          = models.CharField(max_length=20,null=True, blank=True)
    qualification   = models.CharField(max_length=100)
    location        = models.CharField(max_length=50)
    city            = models.CharField(max_length=50)
    paid            = models.BooleanField(default=False)
    min_salary      = models.FloatField(max_length=10)
    max_salary      = models.FloatField(max_length=10)
    duration        = models.CharField(max_length=20,null=True, blank=True)
    min_experience  = models.CharField(max_length=20)
    max_experience  = models.CharField(max_length=20)
    linkedin_id     = models.CharField(max_length=100 ,null=True, blank=True)
    interview_date  = models.DateField(auto_now=False, auto_now_add=False)
    skills          = models.TextField(null=True, blank=True)
    requirements    = models.TextField()
    description     = models.TextField(null=True, blank=True)
    user            = models.ForeignKey(User,on_delete=models.CASCADE)
    status          = models.BooleanField(default=True)
    created_at      = models.DateTimeField(auto_now_add=True)
    updated_at      = models.DateTimeField(auto_now=True)

    # META CLASS
    class Meta:
       
        verbose_name = 'job'
        verbose_name_plural = 'jobs'

    def __str__(self):
        return self.title


class UserJobApplied(models.Model):
    user_id        = models.ForeignKey(User,on_delete=models.CASCADE)
    job_id         = models.ForeignKey(Job,on_delete=models.CASCADE)
    pending        = models.BooleanField(default = True)
    shortlist      = models.BooleanField(default = False)
    select         = models.BooleanField(default = False)
    decline        = models.BooleanField(default = False) 
    remark         = models.TextField(max_length=1000,null=True,blank=True)
  

    class Meta:
        
        verbose_name = 'userjobapplied'
        verbose_name_plural = 'userjobapplieds'

    def __str__(self):
        return self.job_id.title


class Internship(models.Model):
    # id              =models.PositiveIntegerField(primary_key=True,default=datetime.now(),editable=False)
    title           = models.CharField(max_length=120)
    slug            = models.SlugField(unique=True, null=True, blank=True)
    profile         = models.CharField(max_length=50)
    state           = models.CharField(max_length=100)
    city            = models.CharField(max_length=100)
    skills          = models.CharField(max_length=100)
    qualification   = models.CharField(max_length=100)
    start_date      = models.DateField(auto_now=False, auto_now_add=False)
    last_date       = models.DateField(auto_now=False, auto_now_add=False)
    min_stipend     = models.FloatField(max_length=10)
    max_stipend     = models.FloatField(max_length=10)
    duration        = models.CharField(max_length=20)
    description     = models.TextField(max_length=300)
    total_seats     = models.IntegerField(null=True)
    linkedin_id     = models.CharField(max_length=100)
    interview_date  = models.DateField(auto_now=False, auto_now_add=False)
    about_interview = models.TextField(max_length=500)
    quality_council = models.TextField(max_length=1000)
    eligibility     = models.TextField(max_length=1000)
    user            = models.ForeignKey(User,on_delete=models.CASCADE)
    status          = models.BooleanField(default=True)
    created_at      = models.DateTimeField(auto_now_add=True)
    updated_at      = models.DateTimeField(auto_now=True)


    # META CLASS
    class Meta:
        
        verbose_name = 'internship'
        verbose_name_plural = 'internships'

    def __str__(self):
        return self.title



class UserInternshipApplied(models.Model):
    user_id         = models.ForeignKey(User,on_delete=models.CASCADE)
    internship_id   = models.ForeignKey(Internship,on_delete=models.CASCADE)
    pending         = models.BooleanField(default=True)
    shortlist       = models.BooleanField(default=False)
    select          = models.BooleanField(default=False)
    decline         = models.BooleanField(default=False)
    remark          = models.TextField(max_length=1000,null=True,blank=True)


    class Meta:
        
        verbose_name = 'userinternapplied'
        verbose_name_plural = 'userinternapplieds'

    def __str__(self):
        return self.user_id.username






def pre_save_post_receiver(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = unique_slug_generator(instance)


pre_save.connect(pre_save_post_receiver, sender=Job)
pre_save.connect(pre_save_post_receiver, sender=Internship)




