from django.urls import path
from . import views
from . views import *









urlpatterns = [
    path('myhome/',views.myhome,name='myhome'),
    # path('home/',views.home,name='home'),
    path('post_job/',views.post_job,name='post_job'),
    path('posted_job',views.posted_job,name='posted_job'),
    path('update_job/<int:id>/',views.update_job,name='update_job'),
    path('delete_job/<int:id>/',views.delete_job,name='delete_job'),
    path('view_application/<int:id>/',views.view_application,name='view_application'),

    path('post_internship/',views.post_internship,name='post_internship'),
    path('posted_internship',views.posted_internship,name='posted_internship'),
    path('update_internship/<int:id>/',views.update_internship,name='update_internship'),
    path('delete_internship/<int:id>/',views.delete_internship,name='delete_internship'),




]
