from django.shortcuts import render, redirect ,get_object_or_404
from django.http import HttpResponse
from django.views.generic import UpdateView
from .forms import Job_form , Internship_form
from .models import Job , Internship 
from django.views.generic import UpdateView


# Create your views here.
def myhome(request):
    return render(request,'myindex.html',{})
# def home(request):
#     return render(request,'job/dashboard.html',{})

def post_job(request) :
    if request.method=='POST':
       
        form=Job_form(request.POST)
        if form.is_valid():
            
            form.save()
            return redirect('posted_job')
        else:
            print("form not valid")
    form=Job_form()

    return render(request,'job/post_job.html',{'form':form})

def posted_job(request):
    obj=Job.objects.all()
    return render(request,'job/posted_jobs.html',{'obj':obj})

    return render(request,'job/posted_jobs.html',{'obj':obj})
def update_job(request,id):
    obj=user.objects.get(pk=id)
    if request.method=='POST':
        form=Job_form(request.POST,instance=obj)
        if form.is_valid():
            print("form is valid")
            form.save()
            return redirect('posted_job')
        print("form is not valid")
    form=Job_form(instance=obj)
    return render(request,'job/update_job.html',{'form':form})

def delete_job(request,id):
    obj=Job.objects.get(pk=id)
    obj.delete()
    return HttpResponse("Deleted!")

def view_application(request,id):
    obj=Job.objects.get(pk=id)
    return render(request,'job/job_response.html',{'obj':obj})

def post_internship(request):
    form1=Internship_form()
    if request.method=='POST':
        print (" ====post method=== ")
        form1=Internship_form(request.POST)
        if form1.is_valid():
            print (" ===form is valid === ")
            form1.save()
            return redirect('posted_internship')
        else:
            print("form not valid")
    return render(request,'internship/post_internship.html',{'form1':form1})


def posted_internship(request):
    obj=Internship.objects.all()
    return render(request,'internship/posted_internship.html',{'obj':obj})


def update_internship(request,id):
    object = Internship.objects.get(pk=id)
    print (object)
    if request.method == 'POST':
        form = Internship_form(request.POST,instance=object)
        if form.is_valid():
            form.save()
            return redirect('posted_internship')
        print("form is not valid ")
    form = Internship_form(instance=object)
    return render(request, 'internship/update_internship.html', {'form':form})

def delete_internship(request,id):
    obj =internship.objects.get(pk=id)
    obj.delete()
    return HttpResponse("Deleted!")


# class InternshipView(UpdateView):
#     model=Internship
#     template_name='internship/update_internship.html'
#     form_class = Internship_form
#     queryset=Internship.objects.all()
#     def get_object(self):
#         id_=self.kwargs.get('id')
#         return get_object_or_404(Internship,id=id_)
# def delete_internship(request,id):



    

