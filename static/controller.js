$(document).ready(function() {
 let message = $("#status-message");

    // Online Books Like College or AI books
     $("#myjob-form").submit(function (e) {    //----------------------------my edited code-------------------------------
        e.preventDefault();
        console.log('abc')
        let url = $(this).attr("data-action");
        $.ajax({
            url: url,
            method: "POST",
            contentType: false,
            data: new FormData(this),
            processData: false,
            cache: false,
            success: function (data) {
		console.log(data);
                /*let response = JSON.parse(data);
                let responseStatus = response.server[0].status;
                let responseMsg = response.server[0].message;
                responseMessage(responseStatus, responseMsg);
                if (responseStatus === 200) {
                    let id = response.server[0].id;
                    let type = response.server[0].type;
                    window.location.href = '';
                }*/
            },
            error: function (e) {
                console.log("ERROR : ", e);
            }
        });
    });



    function responseMessage(response, msg) {
        if (response === 200) {
            $(message).html(successMsg(msg));
            setTimeout(function () {
                location.replace("https://nerdgeeklab.com");
            }, 2000);
        } else if (response === 500) {
            $(message).html(errorMsg(msg));
        }
    }


    function successMsg(msg) {
        var msg = `
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            ${msg}
        </div>
    `;

        return msg;
    }


    function errorMsg(msg) {
        var msg = `
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            ${msg}
        </div>
    `;
        return msg;
    }

});
